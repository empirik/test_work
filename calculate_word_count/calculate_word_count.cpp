﻿#include <map>
#include <string_view>

std::map<size_t, size_t> calculate_words(std::string_view view, std::string_view separators) {
    auto startIter = view.find_first_not_of(separators);
    auto endIter = view.find_first_of(separators, startIter);
    
    std::map<size_t, size_t> result;
    while (startIter < endIter) {
        auto length = endIter!= std::string_view::npos?  endIter - startIter: view.size()- startIter;
        auto mapIter = result.find(length);
        if (mapIter == result.end()) {
            result.emplace(std::make_pair(length,1));
        } else {
            mapIter->second++;
        }
        startIter = view.find_first_not_of(separators, endIter);
        endIter = view.find_first_of(separators, startIter);
    }
    return result;
}


