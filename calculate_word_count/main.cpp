#include <iostream>
#include "calculate_word_count.h"

int main() {
    for (auto [length, count] : calculate_words("Hello World Empirik!\n")) {
        std::cout << length << " " << count << "\n";
    }
    std::cout << "Hello World!\n";
}