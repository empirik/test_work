#include <gtest/gtest.h>
#include "../calculate_word_count.h"

GTEST_TEST(CALCULATE_WORD_COUNT, NON_WORDS_STRING) {
	for (auto&& zero_word_string : {"","  ","\n\n\n\n \t"}) {
		EXPECT_EQ(calculate_words(zero_word_string).empty(), true)<<"["<< zero_word_string<<"]";
	}
}

GTEST_TEST(CALCULATE_WORD_COUNT, COUNT_WORDS) {
	{
		auto one_word = calculate_words("one");
		ASSERT_FALSE(one_word.empty()) << "map empty";
		ASSERT_TRUE(one_word.find(3) != one_word.end()) << "not found words 3 letters first key " << one_word.begin()->first;
		EXPECT_EQ(one_word[3], 1);
	}
	{
		auto two_word = calculate_words("one second second");
		ASSERT_FALSE(two_word.empty()) << "map empty";
		ASSERT_TRUE(two_word.find(3) != two_word.end() && two_word.find(6) != two_word.end()) << "not found words 3 letters first key " << two_word.begin()->first;
		EXPECT_EQ(two_word[3], 1);
		EXPECT_EQ(two_word[6], 2);
	}
}