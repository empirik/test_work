cmake_minimum_required (VERSION 3.8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")

set(This calculate_word_count_test)

set(Sources
	calculate_word_count_test.cpp
)

add_executable (${This} ${Sources})

target_link_libraries(${This} PUBLIC
	gtest_main
	calculate_word_count_lib
)

add_test(
	NAME ${This}
	COMMAND ${This}
)