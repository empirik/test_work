#include <gtest/gtest.h>
#include "../min_max_numbers.h"

GTEST_TEST(MIN_MAX_NUMBERS_TEST, TEST_NUMBERS) {
	
	auto [zero_min, zero_max] = calculate_min_max(0);
	EXPECT_EQ(zero_min, 0);
	EXPECT_EQ(zero_max, 0);

	auto [min_255, max_255] = calculate_min_max(255);
	EXPECT_EQ(min_255, 255);
}
