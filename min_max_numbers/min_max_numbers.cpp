#include "min_max_numbers.h"

std::pair<uint32_t, uint32_t> calculate_min_max(uint32_t val) {
	uint32_t min_number { 0x00000000 };
	uint32_t max_number { 0x00000000 };
	while (val) {
		max_number >>=  1;
		max_number |= (1 << 31);
		min_number <<= 1;
		min_number |= (1<<0); 
		val &= val - 1;
	}
	return std::make_pair(min_number, max_number);
}

