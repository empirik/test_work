#include <iostream>
#include "min_max_numbers.h"

int main() {
	auto [min_value, max_value] = calculate_min_max(255);
	std::cout << "min value:" << min_value << " max value:" << max_value;
	return 0;
}