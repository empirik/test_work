#include <algorithm>
#include <vector>

namespace my_tree {
	template<typename TPayload> struct TreeNode {
		TPayload payload;
		struct TreeNode* leftChild;
		struct TreeNode* rightChild;
	};

	template<typename TPayload>
	class TreeHolder {
	public:
		using tree_item_type = TreeNode<TPayload>;
		using payload_type = TPayload;
	private:
		tree_item_type* root = nullptr;

		void insert(tree_item_type& now, const payload_type& payload) {
			if (!(now.payload > payload) && !(payload > now.payload)) {
				return;
			}
			if (now.payload > payload) {
				if (now.leftChild == nullptr) {
					auto new_node = new tree_item_type{ payload ,nullptr,nullptr };
					now.leftChild = new_node;
				}
				else {
					insert(*now.leftChild, payload);
				}
			}
			else {
				if (now.rightChild == nullptr) {
					auto new_node = new tree_item_type{ payload ,nullptr,nullptr };
					now.rightChild = new_node;
				}
				else {
					insert(*now.rightChild, payload);
				}
			}
		}

		void clear_childs(tree_item_type& now) {
			if (now.leftChild != nullptr) {
				clear_childs(*now.leftChild);
				delete now.leftChild;
				now.leftChild = nullptr;
			}
			if (now.rightChild != nullptr) {
				clear_childs(*now.rightChild);
				delete now.rightChild;
				now.rightChild = nullptr;
			}
		}

		size_t max_depth(const tree_item_type& now)const {
			return std::max(
				now.leftChild == nullptr ? 0 : 1+max_depth(*now.leftChild),
				now.rightChild == nullptr ? 0 : 1+max_depth(*now.rightChild)
			);
		}

		size_t count_items(const tree_item_type& now)const {
			return 1 +
				(now.leftChild == nullptr ? 0 : count_items(*now.leftChild)) +
				(now.rightChild == nullptr ? 0 : count_items(*now.rightChild));

		}

		void put_max_path(const tree_item_type& now, std::vector<TPayload>& path_container)const {
			path_container.push_back(now.payload);
			if (now.leftChild == nullptr && now.rightChild == nullptr) {
				return;
			}
			if (now.leftChild != nullptr && now.rightChild != nullptr) {
				auto leftDepth = max_depth(*now.leftChild);
				auto rightDepth = max_depth(*now.rightChild);
				put_max_path(leftDepth > rightDepth ? *now.leftChild : *now.rightChild, path_container);
				return;
			}
			put_max_path(now.leftChild != nullptr ? *now.leftChild : *now.rightChild, path_container);
		}


	public:

		void insert(const TPayload& payload) {
			if (root == nullptr) {
				auto new_node = new tree_item_type{ payload ,nullptr,nullptr };
				root = new_node;
				return;
			}
			insert(*root, payload);
		}

		void insert(std::initializer_list<TPayload> payloads) {
			for (auto&& item : payloads) {
				insert(item);
			}
		}

		bool empty()const {
			return root == nullptr;
		}

		size_t size()const {
			if (empty()) {
				return 0;
			}
			return count_items(*root);
		}

		auto get_max_depth_path()const {
			std::vector<TPayload> result;
			if (empty()) {
				return result;
			}
			put_max_path(*root, result);
			return result;
		}

		void clear() {
			if (empty()) {
				return;
			}
			clear_childs(*root);
			delete root;
			root = nullptr;
		}

		~TreeHolder() {
			clear();
		}
	};



}