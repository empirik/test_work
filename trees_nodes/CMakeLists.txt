﻿cmake_minimum_required (VERSION 3.8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


enable_testing()
add_executable (trees_nodes "trees_nodes.cpp" "trees_nodes.h")
add_subdirectory (tests)