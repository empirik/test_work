#include <gtest/gtest.h>
#include "../trees_nodes.h"

GTEST_TEST(TREES_NODES_TEST, TEST_LONG_PATH) {
	my_tree::TreeHolder<size_t> tree;
	ASSERT_TRUE(tree.empty());
	ASSERT_EQ(tree.size(),0);
	{
		auto long_path = tree.get_max_depth_path();
		ASSERT_TRUE(long_path.empty());
	}
	tree.insert({ 5,6,7,8,9,2,3 });
	{
		auto long_path = tree.get_max_depth_path();
		ASSERT_EQ(tree.size(), 7);
		ASSERT_EQ(long_path.size(), 5);
		EXPECT_EQ(long_path[0], 5);
		EXPECT_EQ(long_path[4], 9);
	}
}
