#include <iostream>
#include "trees_nodes.h"

int main() {
	my_tree::TreeHolder<size_t> numbers;
	numbers.insert({ 1,2,3,4,5,6,7,8,9,10});
	std::cout <<" tree size"<< numbers.size()<<"\n";
	auto path = numbers.get_max_depth_path();

	for (auto&& item : path) {
		std::cout << item << " ";
	}
	return 0; 
}
