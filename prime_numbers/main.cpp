#include <iostream>
#include <ctime>
#include "prime_numbers.h"

int main() {
    auto start = clock();

    constexpr auto AIM = 10;
    std::vector<size_t> primeNums(AIM);

    calculate_n_prime(AIM, primeNums);
    std::cout << "Last prime: " << primeNums[AIM - 1];


    auto end = clock();
    std::cout << "\n----------------\n";
    std::cout << "Time: " << double(end - start) / CLOCKS_PER_SEC << " sec \n";
    for (auto&& prime : primeNums) {
        std::cout << prime << ",";
    }
    return 0;
}