﻿#include "prime_numbers.h"
 
 
void calculate_n_prime(size_t AIM, std::vector<size_t>& primeNums) {
    if (primeNums.size() < AIM) {
        primeNums.resize(AIM);
    }
    auto startSize = AIM; // стартовый размер массива натуральных чисел
    auto addSize = AIM; // размер дополнительного массива натуральных чисел

    std::vector<bool> nums(startSize);

    size_t foundPrimes = 0;


    for (decltype(startSize) i = 2; i < startSize; i++) {
        nums[i] = true;
    }

    bool addition = false;
    while (true) {
        if (addition) {
            nums.resize(nums.size() + addSize, true);

            // исключим составные числа простыми из предыдущих итераций
            for (decltype(foundPrimes) i = 0; i < foundPrimes; i++) {
                auto cur_num = primeNums[i];
                if ((addSize + ((nums.size() - addSize) % cur_num)) < cur_num) {
                    continue;
                }
                for (auto j = ((nums.size() - addSize) / cur_num) * cur_num; j < nums.size(); j += cur_num) {
                    nums[j] = false;
                }
            }
        }
        else
            addition = true;


        auto iter = [&foundPrimes, &primeNums] {
            if (foundPrimes == 0) {
                return (size_t)2;
            }
            return primeNums[foundPrimes - 1] + 2;
        }();

        for (; iter < nums.size(); iter++) {
            // выбираем очередное простое число
            if (nums[iter]) {
                primeNums[foundPrimes] = iter;
                foundPrimes++;
                if (foundPrimes == AIM) {
                    break;
                }
                // просеиваем
                for (auto j = iter + iter; j < nums.size(); j += iter) {
                    nums[j] = false;
                }
            }
            else {
                continue;
            }

        }
        if (foundPrimes == AIM)
            break;
    }
}


