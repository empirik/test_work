#include <gtest/gtest.h>
#include "../prime_numbers.h"

GTEST_TEST(PRIME_NUMBERS_TEST,FIRST_5_VALUES) {
	std::vector<size_t> prim_container;
	calculate_n_prime(0, prim_container);
	EXPECT_TRUE(prim_container.empty()) << "non empty values";

	calculate_n_prime(5, prim_container);
	EXPECT_EQ(prim_container[0], 2);
	EXPECT_EQ(prim_container[1], 3);
	EXPECT_EQ(prim_container[2], 5);
	EXPECT_EQ(prim_container[3], 7);
	EXPECT_EQ(prim_container[4], 11);
}