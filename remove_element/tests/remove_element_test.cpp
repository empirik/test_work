#include <gtest/gtest.h>
#include "../remove_element.h"

GTEST_TEST(REMOVE_ELEMENT_TEST, TEST_REMOVE_ODD) {
	my_list::ListHolder<size_t> numbers;

	ASSERT_TRUE(numbers.empty());
	ASSERT_EQ(numbers.size(),0);
	EXPECT_FALSE(numbers.containts(1));

	numbers.push_back(1).push_back(2).push_back(3).push_back(4);

	EXPECT_TRUE(numbers.containts(1));
	EXPECT_TRUE(numbers.containts(2));
	EXPECT_EQ(numbers.size(),4);
	
	numbers.remove_every(2);

	EXPECT_TRUE(numbers.containts(1));
	EXPECT_FALSE(numbers.containts(2));
	EXPECT_EQ(numbers.size(), 2);

}