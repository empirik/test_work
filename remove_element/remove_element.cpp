﻿#include <iostream>

#include "remove_element.h"


int main()
{
	my_list::ListHolder<size_t> vals;
	for (size_t i = 1; i <= 25; i++) {
		vals.push_back(i);
	}

	std::cout << "before delete " << vals.size()<<"\n";
	vals.remove_every(3);
	std::cout << "after delete " << vals.size() << "\n";
	return 0;
}
