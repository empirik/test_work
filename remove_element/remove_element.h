
namespace my_list {
	template<typename TPayload> struct List {
		struct List* next;
		TPayload payload;
	};

	template<typename TPayload> class ListHolder {
	public:
		using payload_type = TPayload;
		using item_type = List<payload_type>;
		
	private:
		item_type* head = nullptr;
		item_type* tail = nullptr;
	public:

		auto& push_back(const payload_type& item) {
			auto newItem = new item_type{ nullptr,item };
			if (head == nullptr) {
				head = newItem;
			}
			else {
				tail->next = newItem;
			}
			tail = newItem;
			return *this;
		}

		bool empty() const {
			return head == tail;
		}
		/**
		 * @brief ��������� ���� �� ������� � ������
		 * @todo �������� ���������� ������ ����������
		 * @param item 
		 * @return true - ���� ������� � ������ 
		*/
		bool containts(const payload_type& item)const {
			auto now = head;
			while (now != nullptr) {
				if (now->payload == item) {
					return true;
				}
				now = now->next;
			}
			return false;
		}

		void clear() {
			auto now = head;
			while (now != nullptr) {
				auto next = now->next;
				delete now;
				now = next;
			}
			head = nullptr;
			tail = nullptr;
		}

		size_t size() const {
			if (head == tail) {
				return 0;
			}
			auto now = head;
			size_t result(1);
			while (now != tail) {
				now = now->next;
				result++;
			}
			return result;
		}

		void remove_every(size_t elem_num) {
			if (elem_num == 0) {
				return;
			}
			if (elem_num == 1) {
				clear();
			}
			auto prev = head;
			auto now = head->next;
			size_t now_elem_num = 2;
			while (now != nullptr) {
				if (now_elem_num % elem_num == 0) {
					prev->next = now->next;
					delete now;
					now = prev->next;

				}
				else {
					prev = now;
					now = now->next;
				}
				now_elem_num++;
			}
			tail = prev;
		}

		~ListHolder() {
			clear();
		}
	};
}